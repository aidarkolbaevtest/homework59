function checkLikes(fa_heart) {
    $.each(fa_heart, function (i, heart) {
        let id = extractNumber(heart.id);
        $.get('/feed/like/check', {id: id}, function (photo) {
            if (photo.isLiked) {
                $(heart).css('color' , 'red')
            } else {
                $(heart).css('color', 'black');
            }
            $('#count' + id).text(photo.count_of_likers);
        })
    })
}