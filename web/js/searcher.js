$(function () {
    let wrapper = $('.wrapper');
    let search_field = $('#search_field');
    let search_button = $('#search_button');
    search_button.on('click', function () {
        $.get('/search', {request: search_field.val()}, function (data) {
            wrapper.html('');
            wrapper.css({
                'width' : '800px',
                'padding' : '5px',
                'margin' : '20px auto',
                'box-shadow' : '0 0 15px gray'
            });
            $.each(data, function (index, user) {
                let div = $(`<div>
                            <div class="search-avatar">
                                <img style="font-size: 0" src="/images/avatars/${user.avatar}" alt="avatar" height="50">
                            </div>
                            <p class="search-username">
                            <a href="/search/user/${user.id}">${user.username}</a>
                            </p>
                            <hr style="clear: both; border: transparent; margin: 0;padding: 0">
                            </div>`);
                div.css({
                    'width': '500px',
                    'margin': '10px auto',
                    'box-shadow' : '0 2px 10px gray',
                    'padding' : '10px',
                    'clear': 'both'
                });
                wrapper.append(div);
            });
        })
    })
});