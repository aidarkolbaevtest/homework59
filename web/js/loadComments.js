function loadComments(data) {
    $.each(data, function (i, data) {
        $.get('/feed/comments/load', {id: data.id}, function (comments) {
            $(data).html('');
            $.each(comments, function (i, comment) {
                let tag = $(`<p style="margin: 5px"><strong>${comment.author}</strong> ${comment.text}</p>`);
                data.append(tag[0]);
            })
        });
    })
}