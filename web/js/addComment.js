$('.comment-field').keypress(function(e) {
    if(e.which === 13 && $(this).val().trim() !== '') {
        let photo_id = extractNumber(this.id);
        $.post('/feed/comments/add', {id: photo_id, text: $(this).val()});
        let divData = $(`#${photo_id}`);
        loadComments(divData);
        $(this).val('');
    }
});