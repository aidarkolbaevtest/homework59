<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @Vich\Uploadable
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="avatar_file", fileNameProperty="avatar")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar", type="text", nullable=true, unique=false)
     */
    private $avatar;

    /**
     *
     * @ORM\OneToMany(targetEntity="Photo", mappedBy="author", cascade={"persist", "remove"})
     */
    private $photos;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Comment", mappedBy="author", cascade={"persist", "remove"})
     */
    private $comments;

    /**
     * @ORM\ManyToMany(targetEntity="User")
     */
    private $followers;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Photo", mappedBy="likers")
     */
    private $likedPhotos;

    public function __construct()
    {
        parent::__construct();
        $this->photos = new ArrayCollection();
        $this->likedPhotos = new ArrayCollection();
        $this->followers = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return User
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function __toString()
    {
        return $this->username ?: '';
    }

    /**
     * @param User $follower
     */
    public function addFollowers(User $follower)
    {
        $this->followers->add($follower);
    }

    /**
     * @return mixed
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    /**
     * @param string $avatar
     * @return User
     */
    public function setAvatar($avatar): User
    {
        $this->avatar = $avatar;
        return $this;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @return mixed
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * @param Photo $photo
     */
    public function removePhoto(Photo $photo)
    {
        $this->photos->removeElement($photo);
    }

    /**
     * @param Photo $photo
     */
    public function addPhoto(Photo $photo)
    {
        $this->photos->add($photo);
    }

    /**
     * @return ArrayCollection
     */
    public function getLikedPhotos(): ArrayCollection
    {
        return $this->likedPhotos;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isFollowing(User $user)
    {
        return $this->followers->contains($user);
    }

    /**
     * @param User $user
     */
    public function unsubscribe(User $user)
    {
        $this->followers->removeElement($user);
    }

    /**
     * @return mixed
     */
    public function getComments()
    {
        return $this->comments;
    }
}
