<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Photo
 *
 * @ORM\Table(name="photo")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PhotoRepository")
 * @Vich\Uploadable
 */
class Photo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;


    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="photo_file", fileNameProperty="photo")
     *
     * @var File
     */
    private $photoFile;



    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $photo;
    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="photos")
     * @ORM\JoinColumn(name="author", referencedColumnName="id")
     */
    private $author;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Comment", mappedBy="photo", cascade={"persist", "remove"})
     */
    private $comments;

    /**
     * @var ArrayCollection
     *
     *  @ORM\ManyToMany(targetEntity="User", inversedBy="likedPhotos")
     */
    private $likers;


    public function __construct()
    {
        $this->likers = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }



    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setPhotoFile(?File $image = null): void
    {
        $this->photoFile = $image;

        if (null !== $image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getPhotoFile(): ?File
    {
        return $this->photoFile;
    }


    public function setPhoto(?string $photo): void
    {
        $this->photo = $photo;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set description
     *
     * @param string $description
     *
     * @return Photo
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param User $author
     * @return Photo
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @return ArrayCollection
     */
    public function getLikers()
    {
        return $this->likers;
    }

    public function getCountOFLikers()
    {
        return count($this->likers);
    }

    /**
     * @param User $user
     */
    public function addLiker(User $user)
    {
        $this->likers->add($user);
    }

    /**
     * @param User $user
     */
    public function removeLiker(User $user) {
        $this->likers->removeElement($user);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function hasLiker(User $user) {
        return $this->likers->contains($user);
    }

    /**
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return date_format($this->updatedAt, 'd-m-Y H:i');
    }

    /**
     * @return mixed
     */
    public function getComments()
    {
        return $this->comments;
    }
}

