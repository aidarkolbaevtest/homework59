<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use \Doctrine\Common\Collections\Criteria;

/**
 * @Route("/feed")
 * Class FeedController
 * @package AppBundle\Controller
 */
class FeedController extends Controller
{
    /**
     * @Route("/", name="feed_action")
     */
    public function indexAction()
    {
        $users = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findAll();
        $array_of_id = [];
        foreach ($users as $user) {
            if ($user->isFollowing($this->getUser())) {
                $array_of_id[] = $user->getId();
            }
        }

        $criteria = new Criteria();
        $criteria->where($criteria->expr()->in('author', $array_of_id))->orderBy(['updatedAt' => $criteria::DESC]);

        $photoRepository = $this->getDoctrine()->getRepository('AppBundle:Photo');
        $result = $photoRepository->matching($criteria);
        return $this->render('@App/Feed/index.html.twig', array(
            'photos' => $result
        ));
    }

    /**
     * @Route("/comments/load")
     * @Method("GET")
     * @param Request $request
     * @return JsonResponse
     */
    public function loadComments(Request $request)
    {
        $photo = $this->getDoctrine()->getRepository('AppBundle:Photo')->find($request->get('id'));
        $comments = $photo->getComments();
        $result = [];
        foreach ($comments as $comment) {
            $result[] = [
                'author' => $comment->getAuthor()->getUsername(),
                'text' => $comment->getText()
            ];
        }
        return new JsonResponse($result);
    }


    /**
     * @Route("/comments/add")
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     */
    public function addComment(Request $request)
    {
        $comment = new Comment();
        $comment->setAuthor($this->getUser());
        $comment->setText($request->get('text'));
        $comment->setPhoto($this->getDoctrine()->getRepository('AppBundle:Photo')->find($request->get('id')));
        $em = $this->getDoctrine()->getManager();
        $em->persist($comment);
        $em->flush();

        return new JsonResponse([
            //
        ]);
    }



    /**
     * @Route("/like")
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     */
    public function likeAction(Request $request)
    {
        $photo = $this->getDoctrine()->getRepository('AppBundle:Photo')->find($request->get('id'));
        $user = $this->getUser();
        if ($photo->hasLiker($user)) {
            $photo->removeLiker($user);
        } else {
            $photo->addLiker($user);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($photo);
        $em->flush();

        return new JsonResponse([
            //
        ]);
    }


    /**
     * @Route("/like/check")
     * @Method("GET")
     * @param Request $request
     * @return JsonResponse
     */
    public function checkLikeAction(Request $request) {
        $photo = $this->getDoctrine()->getRepository('AppBundle:Photo')->find($request->get('id'));
        return new JsonResponse([
            'isLiked' => $photo->hasLiker($this->getUser()),
            'count_of_likers' => $photo->getCountOFLikers()
        ]);
    }

}
