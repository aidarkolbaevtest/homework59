<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Photo;
use AppBundle\Form\PhotoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/profile")
 * Class ProfileController
 * @package AppBundle\Controller
 */
class ProfileController extends Controller
{
    /**
     * @Route("/", name="user_profile")
     */
    public function profileAction()
    {
        $user = $this->getUser();
        $count_of_followers = count($user->getFollowers());
        $count_of_photos = count($user->getPhotos());
        $photos = $user->getPhotos();
        // сортировка объектов
        $sorted_photos = [];
        for ($i = (count($photos) - 1);$i >= 0; $i-- ) {
            $sorted_photos[] = $photos[$i];
        }
        return $this->render('@App/Profile/profile.html.twig', array(
            'user' => $user,
            'count_of_followers' => $count_of_followers,
            'count_of_photos' => $count_of_photos,
            'photos' => $sorted_photos
        ));
    }

    /**
     * @Route("/new/photo")
     * @Method({"GET", "HEAD", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newPhotoAction(Request $request)
    {
        $photo = new Photo();
        $form = $this->createForm(PhotoType::class, $photo, ['method' => 'POST']);

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $photo = $form->getData();
            $photo->setAuthor($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($photo);
            $em->flush();
            return $this->redirectToRoute('user_profile');
        }

        return $this->render('@App/Profile/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/follow")
     * @Method("GET")
     * @param Request $request
     * @return JsonResponse
     */
    public function subscribeAction(Request $request)
    {
        $following_user = $this->getDoctrine()->getRepository('AppBundle:User')->find($request->get('id'));
        if ($following_user->isFollowing($this->getUser())) {
            $following_user->unsubscribe($this->getUser());
        } else {
            $following_user->addFollowers($this->getUser());
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($following_user);
        $em->flush();

        return new JsonResponse([
            //
        ]);
    }

    /**
     * @Route("/follow/check")
     * @Method("GET")
     * @param Request $request
     * @return JsonResponse
     */
    public function checkSubscribeAction(Request $request) {
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($request->get('id'));
        return new JsonResponse([
            'isFollowing' => $user->isFollowing($this->getUser())
        ]);
    }
}
