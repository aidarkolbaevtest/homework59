<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SearchController
 * @Route("/search")
 */
class SearchController extends Controller
{
    /**
     * @Route("/")
     * @Method("GET")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchAction(Request $request)
    {
        $users = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();
        $response_data = [];
        foreach ($users as $user) {
            if (preg_match_all('#[' . $request->get('request') . ']#',
                $user->getUsername()) >= 2 && $user->getUsername() !== $this->getUser()->getUsername())
            {
                $response_data[] = [
                    'username' => $user->getUsername(),
                    'avatar' => $user->getAvatar(),
                    'id' => $user->getId()
                ];
            }
        }


        return new JsonResponse($response_data);
    }

    /**
     * @Route("/user/{id}", requirements={"id": "\d+"})
     * @Method({"GET","HEAD"})
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsAction(int $id) {
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);
        $count_of_followers = count($user->getFollowers());
        $count_of_photos = count($user->getPhotos());
        $photos = $user->getPhotos();
        return $this->render('@App/Profile/details.html.twig', array(
            'user' => $user,
            'count_of_followers' => $count_of_followers,
            'count_of_photos' => $count_of_photos,
            'photos' => $photos
        ));
    }

}
